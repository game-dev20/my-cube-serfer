﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    [SerializeField] private GameObject _startPanel;
    [SerializeField] private GameObject _finishPanel;
    [SerializeField] private Text _messageFinishGame;

    static public LevelController instance;

    private bool _isCompletingLevel = false;
    private bool _isStartGame = false;

    private void Start()
    {
        instance = this;

        _startPanel.SetActive(true);
        _finishPanel.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && !_isCompletingLevel && _isStartGame)
        {
            StartMovementPlayer();
        }
    }

    private void StartMovementPlayer()
    {
        SidewaysMotionControl.isCanMoveSideways = true;
        ForwardMotionControl.isCanMoveForward = true;
    }

    private void StopMovementPlayer()
    {
        SidewaysMotionControl.isCanMoveSideways = false;
        ForwardMotionControl.isCanMoveForward = false;
    }

    public void LoseGame()
    {
        _isCompletingLevel = true;
        StopMovementPlayer();

        _messageFinishGame.text = "Вы проиграли!";
        _finishPanel.SetActive(true);

        Debug.Log("Lose Game.");
    }

    public void WinGame()
    {
        _isCompletingLevel = true;
        StopMovementPlayer();

        _messageFinishGame.text = "Вы выиграли!";
        _finishPanel.SetActive(true);

        Debug.Log("Win Game.");
    }

    public void StartGame()
    {
        _isStartGame = true;

        _startPanel.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Level");
    }
}
