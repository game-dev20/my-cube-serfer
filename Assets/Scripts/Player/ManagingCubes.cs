﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagingCubes : MonoBehaviour
{
    public Vector3 offsetDown = new Vector3(0f, -1f, 0f);
    public float amountPaddingLossCube = 0.2f;
    public float coordinateYDeletingCube = 0f;
    public Transform[] alreadyAttachedCubes;

    private int _totalNumberCubesAttached = 0;
    private List<Transform> _joiningCubes = new List<Transform>();

    private BoxCollider _collider;
    private Transform _transform;

    private void Start()
    {
        _collider = GetComponent<BoxCollider>();
        _transform = transform;

        foreach(Transform cude in alreadyAttachedCubes)
        {
            _totalNumberCubesAttached++;
            _joiningCubes.Add(cude);
        }
    }

    private void Update()
    {
        List<Transform> cubesRemove = new List<Transform>();
        foreach (Transform joiningCube in _joiningCubes)
        {
            Vector3 localPosCube = joiningCube.localPosition;
            Vector3 gloabalPosCube = joiningCube.position;
            float localPosY = localPosCube.y;
            float globalPosY = gloabalPosCube.y;

            if (
                (Mathf.Abs(localPosCube.x) >= amountPaddingLossCube) ||
                (Mathf.Abs(localPosCube.z) >= amountPaddingLossCube) ||
                (globalPosY < coordinateYDeletingCube)
            )
            {
                cubesRemove.Add(joiningCube);
                joiningCube.parent = null;
            }
            else
            {
                joiningCube.localPosition = new Vector3(0f, localPosY, 0f);
            }
        }
        foreach (Transform cube in cubesRemove)
        {
            _joiningCubes.Remove(cube);

            float globalPosY = cube.position.y;
            if (globalPosY < coordinateYDeletingCube)
            {
                Destroy(cube.gameObject);
            }

            if (_joiningCubes.Count == 0)
            {
                if (LevelController.instance != null)
                {
                    LevelController.instance.LoseGame();
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Transform transformOther = other.transform;

        JoiningCube joiningCube = null;
        joiningCube = transformOther.GetComponent<JoiningCube>();

        if (joiningCube != null)
        {
            if (!joiningCube.isJoining)
            {
                joiningCube.isJoining = true;

                _totalNumberCubesAttached++;

                _transform.Translate((-1f) * offsetDown);

                _joiningCubes.Add(transformOther);
                transformOther.parent = _transform;
                Vector3 localPosJoiningCube = transformOther.localPosition;
                float localPosY = localPosJoiningCube.y;
                transformOther.localPosition = new Vector3(0f, localPosY, 0f);

                SetParamsCollider();
            }
        }
    }

    private void SetParamsCollider()
    {
        float offsetCenterY = (-1) * (_totalNumberCubesAttached / 2f - 0.5f);
        Vector3 centerCollider = _collider.center;
        centerCollider.y = offsetCenterY;

        float sizeColliderY = _totalNumberCubesAttached + 0.1f;
        Vector3 sizeCollider = _collider.size;
        sizeCollider.y = sizeColliderY;

        _collider.size = sizeCollider;
        _collider.center = centerCollider;
    }
}
