﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidewaysMotionControl : MonoBehaviour
{
    public float lateralSpeed = 1.0f;

    public float maxDisplacementLeft = -2.0f;
    public float maxDisplacementRight = 2.0f;

    static public bool isCanMoveSideways = false;

    private Transform _transform;

    private void Start()
    {
        _transform = transform;
    }

    private void Update()
    {
        SidewaysMovement();
    }

    private void SidewaysMovement()
    {
        if (!isCanMoveSideways)
        {
            return;
        }

        float moveLateral = 0f;
        if (Input.GetMouseButton(0))
        {
            moveLateral = Input.GetAxis("Mouse X") * lateralSpeed;
        }
        
        Vector3 moving = Vector3.right * moveLateral;

        _transform.Translate(moving);

        Vector3 localPosition = _transform.localPosition;
        float localX = localPosition.x;
        float localY = localPosition.y;
        float localZ = localPosition.z;
        if (localX < maxDisplacementLeft)
        {
            _transform.localPosition = 
                new Vector3(maxDisplacementLeft, localY, localZ);
        }
        else if (localX > maxDisplacementRight)
        {
            _transform.localPosition = 
                new Vector3(maxDisplacementRight, localY, localZ);
        }
    }
}
