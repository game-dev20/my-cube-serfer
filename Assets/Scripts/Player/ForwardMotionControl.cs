﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;

public class ForwardMotionControl : MonoBehaviour
{
    static public bool isCanMoveForward = false;

    private BezierWalkerWithSpeed _bezierWalkerWithSpeed;

    private void Start()
    {
        _bezierWalkerWithSpeed = GetComponent<BezierWalkerWithSpeed>();
    }

    private void Update()
    {
        if (_bezierWalkerWithSpeed != null)
        {
            _bezierWalkerWithSpeed.enabled = isCanMoveForward;
        }
    }
}
